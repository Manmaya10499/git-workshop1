public class Orthopedician extends Doctor {
	@Override
	public void treatPatient() {
		System.out.println("treat patient of Orthopedician");
		ctScan();
		conductXray();
	}
	public void ctScan() {
		System.out.println("conducting ct scan");
	}
	public void conductXray(){
		System.out.println("Conducting x-ray");
	}
          
}
