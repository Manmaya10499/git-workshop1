class Test2{
    public static void main(String[] args) {
        method1();
        System.out.println("main method call is over");
    }
    public static void method1(){
        System.out.println("before calling method 1");
        method2();
        System.out.println("method 1 executed");
    }
    public static void method2(){
        System.out.println("before calling method 2");
        method3();
        System.out.println("method 2 executed");
    }
    public static void method3(){
        System.out.println("method 3 executed");
    }
}