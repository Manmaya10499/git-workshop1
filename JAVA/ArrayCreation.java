class ArrayCreation{
    public static void populateArray(int row,int col,int value,int arr[][]){
        for(int i=0;i<row;i++){
            for(int j=0;j<col;j++){
                arr[i][j]=value++;
            }
        }
    }
    public static void displayArray(int IntArr[][]){
        for(int i=0;i<IntArr.length;i++){
            for(int j=0;j<IntArr[i].length;j++){
                System.out.printf(" %d  ",IntArr[i][j]);
            }
            System.out.println();
         }
    }
    public static void main(String[] args) {
        int row=4,col=5,startValue=10;
        int arr[][]=new int[row][col];
        populateArray(row,col,startValue,arr);
        displayArray(arr);
    }
}