class PrimitiveDataTypes{
    public static void main(String[] args) {
        byte b=120;
        short s=2_2_2;
        int i=67_46_1;
        int i2=111;
        long b4=6_23_85_65_6L;
        byte b3=(byte)i2;
        boolean b2=false;
        char c='C';
        float f=635.75f;
        double d=64.45_56__47;
    }
}