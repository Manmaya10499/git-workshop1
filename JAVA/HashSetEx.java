import java.util.*;

//class AccSortById implements Comparator<SavingsAccount> {
//	@Override
//	public int compare(SavingsAccount o1, SavingsAccount o2) {
//		return (int) (o1.AccountNumber() - o2.AccountNumber());
//	}
//}
//
//class AccSortByIdReverse implements Comparator<SavingsAccount> {
//	@Override
//	public int compare(SavingsAccount o1, SavingsAccount o2) {
//		return (-1) * (int) (o1.AccountNumber() - o2.AccountNumber());
//	}
//}
//
//class AccSortByName implements Comparator<SavingsAccount> {
//	@Override
//	public int compare(SavingsAccount o1, SavingsAccount o2) {
//		return o1.AccountHolderName().compareTo(o2.AccountHolderName());
//	}
//
//}

public class HashSetEx {

	public static void main(String[] args) {

//		AccSortById obj = new AccSortById();
		Set<SavingsAccount> set = null;
		SavingsAccount acc1 = new SavingsAccount("manmaya");
		SavingsAccount acc2 = new SavingsAccount("chinmaya");
		SavingsAccount acc3 = new SavingsAccount("tanmaya");
		SavingsAccount acc4 = new SavingsAccount("chinmaya");
		SavingsAccount acc5 = new SavingsAccount("happy");
		SavingsAccount acc6 = new SavingsAccount("tushar");
		SavingsAccount acc7 = new SavingsAccount("chitan");

		System.out.println("enter a number");
		
		Comparator<SavingsAccount>  AccSortById= (o1,o2) ->  
		(int) (o1.AccountNumber()-o2.AccountNumber());
		
		Comparator<SavingsAccount>  AccSortByIdReverse= (o1,o2) ->  
		(-1)*(int)(o1.AccountNumber()-o2.AccountNumber());
		  
		Comparator<SavingsAccount>  AccSortByName= (o1,o2) ->  
		o1.AccountHolderName().compareTo(o2.AccountHolderName());
		
		Comparator<SavingsAccount>  AccSortByNameReverse= (o1,o2) ->  
		o1.AccountHolderName().compareTo(o2.AccountHolderName());
		
		Scanner sc = new Scanner(System.in);
		switch (sc.nextInt()) {
		case 1:
			set = new TreeSet<>(AccSortById);
			break;
		case 2:
			set = new TreeSet<>(AccSortByIdReverse);
			break;
		case 3:
			set = new TreeSet<>(AccSortByName);
			break;
		case 4:
			set = new TreeSet<>(AccSortByNameReverse);
			break;
		default:
			System.out.println("wrong input");
			break;
		}

		set.add(acc1);
		set.add(acc2);
		set.add(acc3);
		set.add(acc4);
		set.add(acc5);
		set.add(acc6);
		set.add(acc7);

//		if(acc2.equals(acc4)){
//			System.out.println("EQUAL "+acc2+ " " +acc4);
//		}
//		else {
//			System.out.println("not equal");
//		}
//		Iterator<SavingsAccount> it=set.iterator();
//		
//		while(it.hasNext()) {
//			System.out.println(it.next().AccountNumber());
//		}

//		System.out.println(obj.compare(acc1,acc2));

		System.out.println(set);
	}

}
