import java.util.*;
class BankAccRepository implements EmployeeDAO{
	
    private  SavingsAccount accounts[]=new SavingsAccount[10];
    SavingsAccount acc[]=accounts;
    private  List<SavingsAccount> accountsNew=new ArrayList<>();
    List<SavingsAccount> list = Arrays.asList(accounts);
    private static int index=0;
    public void addBankAccount(SavingsAccount acc){
    this.accounts[index++]=acc;
    }
    public SavingsAccount[] listAllAccount(){
        int i=0,k=0;
        while(accounts[i++]!=null)
        k++;
        SavingsAccount arr[]=new SavingsAccount[k];
        System.out.println(k);
        i=0;
        while(i<k){
        arr[i]=accounts[i];
        i++;
        }
        return arr;
    }
    public SavingsAccount getAccount(int index){
       return accounts[index];
    }
    public SavingsAccount deleteAccount(int index){
       SavingsAccount acc=accounts[index];
       accounts[index]=null;
       System.out.println("account deleted");
       return acc;
    }
    
    public SavingsAccount getAccountByAccountNumber(long accNo){
        SavingsAccount s=null;
        for (SavingsAccount sv : accounts) {
            if(sv.AccountNumber()==accNo){
                s=sv;
                break;
            }
        }
        return s;
    }
    
	@Override
	public ArrayList<SavingsAccount> listAllAccount2() {
		if(acc!=null) {
			Collections.addAll(accountsNew, acc);
			acc=null;
		}
		return (ArrayList<SavingsAccount>) accountsNew;
	}
	@Override
	public SavingsAccount getAccount2(int index) {
		return accountsNew.get(index);
	}
	@Override
	public SavingsAccount deleteAccount2(int index) {
		SavingsAccount obj=accountsNew.get(index);
		accountsNew.remove(index);
		return obj;
	}
	@Override
	public SavingsAccount getAccountByAccountNumber2(long accNo) {
		 SavingsAccount s=null;
	        for (SavingsAccount sv : accountsNew) {
	            if(sv.AccountNumber()==accNo){
	                s=sv;
	                break;
	            }
	        }
	        return s;
	}
	@Override
	public void addBankAccount2(SavingsAccount acc) {
		accountsNew.add(acc);
	}
	
	
	
}