public abstract class BankAccount {
	private int acc_no;
	private String name;
	private double balance;
	public BankAcc(int acc_no, String name){
		this.acc_no=acc_no;
		this.name=name;
		this.balance=0;
	}
	public abstract double deposit(double amount);
	public abstract double withdraw(double amount);
	


}
