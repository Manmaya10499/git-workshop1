import java.util.ArrayList;

class AccountHolder {
	public static void main(String[] args) {
		BankAccRepository repo = new BankAccRepository();
		SavingsAccount acc1 = new SavingsAccount("manamya", 10000);
		repo.addBankAccount(acc1);
		System.out.println("NAME  " + acc1.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc1.AccountNumber());
		System.out.println("BALANCE  " + acc1.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc1.withdraw(5000));
		System.out.println("AMOUNT DEPOSITED  " + acc1.deposit(10000, "for tution fees:-"));
		System.out.println("BALANCE  " + acc1.checkBalance());
		System.out.println("------------------------------");

		SavingsAccount acc2 = new SavingsAccount("chinmaya", 1100);
		repo.addBankAccount(acc2);
		System.out.println("NAME  " + acc2.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc2.AccountNumber());
		System.out.println("BALANCE  " + acc2.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc2.withdraw(5000));
		System.out.println("AMOUNT DEPOSITED  " + acc2.deposit(10000, "for travelling:-"));
		System.out.println("BALANCE  " + acc2.checkBalance());
		System.out.println("------------------------------");

		SavingsAccount acc3 = new SavingsAccount("tanmaya", 5500);
		repo.addBankAccount(acc3);
		System.out.println("NAME  " + acc3.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc3.AccountNumber());
		System.out.println("BALANCE  " + acc3.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc3.withdraw(5000));
		System.out.println("AMOUNT DEPOSITED  " + acc3.deposit(1550000));
		System.out.println("BALANCE  " + acc3.checkBalance());

		System.out.println("------------------------------");

		SavingsAccount acc4 = new SavingsAccount("tushar");
		repo.addBankAccount(acc4);

		System.out.println("NAME  " + acc4.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc4.AccountNumber());
		System.out.println("BALANCE  " + acc4.checkBalance());
		System.out.println("AMOUNT DEPOSITED  " + acc4.deposit(5500, "for buying cloths"));
		System.out.println("BALANCE  " + acc4.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc4.withdraw(5000));
		System.out.println("BALANCE  " + acc4.checkBalance());

		System.out.println("------------------------------");

		SavingsAccount acc5 = new SavingsAccount("adarsh");
		repo.addBankAccount(acc5);
		System.out.println("NAME  " + acc5.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc5.AccountNumber());
		System.out.println("BALANCE  " + acc5.checkBalance());
		System.out.println("AMOUNT DEPOSITED  " + acc5.deposit(35500));
		System.out.println("BALANCE  " + acc5.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc5.withdraw(15000));
		System.out.println("BALANCE  " + acc5.checkBalance());

		System.out.println("------------------------------");

		SavingsAccount acc6 = new SavingsAccount("asish");
		repo.addBankAccount2(acc6);
		System.out.println("NAME  " + acc6.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc6.AccountNumber());
		System.out.println("BALANCE  " + acc6.checkBalance());
		System.out.println("AMOUNT DEPOSITED  " + acc6.deposit(35500));
		System.out.println("BALANCE  " + acc6.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc6.withdraw(15000));
		System.out.println("BALANCE  " + acc6.checkBalance());

		System.out.println("------------------------------");

		SavingsAccount acc7 = new SavingsAccount("subhakant", 15000);
		repo.addBankAccount2(acc7);
		System.out.println("NAME  " + acc7.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc7.AccountNumber());
		System.out.println("BALANCE  " + acc7.checkBalance());
		System.out.println("AMOUNT DEPOSITED  " + acc7.deposit(35500));
		System.out.println("BALANCE  " + acc7.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc7.withdraw(15000));
		System.out.println("BALANCE  " + acc7.checkBalance());

		System.out.println("------------------------------");

		SavingsAccount acc8 = new SavingsAccount("abhisek", 22000);
		repo.addBankAccount2(acc8);
		System.out.println("NAME  " + acc8.AccountHolderName());
		System.out.println("ACCOUNT NO.  " + acc8.AccountNumber());
		System.out.println("BALANCE  " + acc8.checkBalance());
		System.out.println("AMOUNT DEPOSITED  " + acc8.deposit(35500));
		System.out.println("BALANCE  " + acc8.checkBalance());
		System.out.println("AMOUNT WITHDRAWN  " + acc8.withdraw(15000));
		System.out.println("BALANCE  " + acc8.checkBalance());
		System.out.println("------------------------------");

		System.out.println("the account at index 2 is " + repo.getAccount2(0).AccountHolderName());
		System.out.println("account got deleted at index 1 " + repo.deleteAccount2(1).AccountHolderName());
		System.out.println("account info of acc no 1005 " + repo.getAccountByAccountNumber2(1005).AccountHolderName());

		SavingsAccount arr[] = repo.listAllAccount();
		System.out.println("ACCOUNT HOLDERS ARE  ");

//		for (int i = 0; i < arr.length; i++)
//			System.out.println(arr[i].AccountHolderName() + "\t" + arr[i].AccountNumber());
//
//		System.out.println("(newly added accounts)");

		ArrayList<SavingsAccount> sv = repo.listAllAccount2();
		for (SavingsAccount savingsAccount : sv) {
			System.out.println(savingsAccount.AccountHolderName() + "\t" + savingsAccount.AccountNumber());
		}

//        System.out.println(repo.getAccountByAccountNumber(1001).AccountHolderName());
	}
}