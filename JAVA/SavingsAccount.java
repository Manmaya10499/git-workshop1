import java.util.*;
class SavingsAccount implements Comparable<SavingsAccount> {
	private String name;
	private double balance;
	private long accNumber;
	private static long count = 1000;

	public SavingsAccount(String name, double initialBal) {
		this.name = name;
		this.balance = initialBal;
		this.accNumber = count++;
	}

	public SavingsAccount(String name) {
		this.name = name;
		this.accNumber = count++;
	}

	public double deposit(double value) {
		this.balance += value;
		return value;
	}

	public double deposit(double value, String msg) {
		this.balance += value;
		System.out.println(msg);
		return value;
	}

	public double withdraw(double value) {
		if (value < this.balance) {
			this.balance = this.balance - value;
			return value;
		}
		System.out.println("insufficient balance");
		return 0;
	}

	public double checkBalance() {
		return this.balance;
	}

	public String AccountHolderName() {
		return this.name;
	}
	public long AccountNumber() {
		return this.accNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (accNumber ^ (accNumber >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SavingsAccount other = (SavingsAccount) obj;
		if (accNumber != other.accNumber)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return this.AccountHolderName()+this.AccountNumber();
	}

	@Override
	public int compareTo(SavingsAccount o) {
		return this.name.compareTo(o.name);
	}
	
}