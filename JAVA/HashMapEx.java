import java.util.*;
import java.util.Map.Entry;

public class HashMapEx {

	public static void main(String[] args) {
		SavingsAccount acc1 = new SavingsAccount("xyz", 10000);
		SavingsAccount acc2 = new SavingsAccount("abc", 20000);
		SavingsAccount acc3 = new SavingsAccount("mno", 30000);
		SavingsAccount acc4 = new SavingsAccount("jkl", 40000);
		Map<Long, SavingsAccount> acc = new HashMap();
		acc.put(101l, acc1);
		acc.put(102l, acc2);
		acc.put(103l, acc3);
		acc.put(104l, acc4);
		acc.put(104l, acc1);

		Set<Long> setKeys = acc.keySet();
		
		
//		System.out.println(setKeys);
//		System.out.println(acc.get(101l));
		System.out.println(acc);
		
		Iterator<Long> it = setKeys.iterator();
		
		while (it.hasNext()) {
			System.out.println(acc.get(it.next()));
		}
		
		Set<Map.Entry<Long, SavingsAccount>> set = acc.entrySet();
		Iterator<Map.Entry<Long, SavingsAccount>> it2 = set.iterator();

		while (it2.hasNext()) {
			System.out.println(it2.next());
		}
		
		
//        Set<Entry<Long, SavingsAccount>> entrySet = acc.entrySet();
//        for (Map.Entry<Long, SavingsAccount> entry : entrySet) {
//			
//		}
//        System.out.println(entrySet);

	}

}
