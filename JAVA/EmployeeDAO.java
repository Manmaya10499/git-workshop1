import java.util.ArrayList;
public interface EmployeeDAO {
	public void addBankAccount2(SavingsAccount acc);
	public ArrayList<SavingsAccount> listAllAccount2();
	public SavingsAccount getAccount2(int index);
	public SavingsAccount deleteAccount2(int index);
	public SavingsAccount getAccountByAccountNumber2(long accNo);
	
}
